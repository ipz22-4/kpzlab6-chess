﻿namespace ChessBoard.ClassLibrary
{
    public interface IAttackInfo
    {
        bool IsAttackPiece { get; set; }
        bool IsAttackPathWhite { get; set; }
        bool IsAttackPathBlack { get; set; }
    }
}
